import React, { Component } from 'react'
import ProductItem from '../product-item';
import './product-list.css';


export default class ProductList extends Component{
  renderItems(arr){
    return arr.map((item) => {
      const { id, ...itemProps } = item;
      return (
        <li key={id} className="product-list-item-group">
          <ProductItem 
            data={itemProps} 
            onAddToCart={() => this.props.onAddToCart(item)} 
            onChooseItem={() => this.props.onChooseItem(id)}/>
        </li>
      );
    });
  }

  render(){
    const { data } = this.props;
    return (
      <ul className="product-list">
          {this.renderItems(data)}
      </ul>
    )
  }
}