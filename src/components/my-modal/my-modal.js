import React, { Component } from 'react';
import './my-modal.css';

export default class MyModal extends Component{
    render(){
        const { showModal, onCloseModal, onAddToCartConfirm} = this.props;
        const display = showModal ? "block" : "none";
        return (
            <div className="modal" tabIndex="-1" role="dialog" style={{ display }}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Додати в корзину</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={() => onCloseModal()}>
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>Ви впевнені що хочете додати об*єкт в корзину?</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" onClick={ () => onAddToCartConfirm()}>Додати в корзину</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={() => onCloseModal()}>Close</button>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}