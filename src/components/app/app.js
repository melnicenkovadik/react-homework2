import React, { Component } from 'react';
import ProductList from '../product-list';
import MyModal from '../my-modal'
import './app.css';
import axios from 'axios';

const getProducts = () => {
  return axios.get('products.json');
}

const writeToLocalStorage = (key, value) => {
  const keyValue = localStorage.getItem(key);
  if(keyValue && keyValue.length !== 0){
    const localStorageArray = JSON.parse(keyValue);
    localStorageArray.push(value);
    localStorage.setItem(key, JSON.stringify(localStorageArray));
  } else {
    localStorage.setItem(key, JSON.stringify([value]));
  }
}

const deleteFromLocalStorage = (key, valueToDelete) => {
  const keyValue = localStorage.getItem(key);
  if(keyValue){
    const localStorageArray = JSON.parse(keyValue);
    const index = localStorageArray.indexOf(valueToDelete);
    if (index > -1) {
      localStorageArray.splice(index, 1);
      localStorage.setItem(key, JSON.stringify(localStorageArray));
    }
  }
}

export default class App extends Component{
  state ={
    data: null,
    modal: false,
    cartItem: null
  }

  componentDidMount(){
    getProducts().then((response) => {
      let data = response.data;
      const chosenProducts = JSON.parse(localStorage.getItem("chosen"));
      //Set chosen products from localStorage
      if(chosenProducts){
        data = data.map((item) => {
          if(chosenProducts.includes(item.id)){
            item.isChosen = true;
          }
          return item;
        });
      }
      return data;
    })
    .then((result) => {
      this.setState({
        data: result
      });
    })
  }

  onAddToCart = (item) => {
    this.setState({
      cartItem: item,
      modal: true
    })
  }


  toogleChooseItem = (id) => {
    this.setState(({ data }) => {
      const idx = data.findIndex((el) => el.id === id);

      const oldItem = data[idx];
      const newItem = { ...oldItem, ["isChosen"]: !oldItem["isChosen"] };

      if(newItem.isChosen == false){
        deleteFromLocalStorage("chosen", newItem.id);
      }
      else{
        writeToLocalStorage("chosen", newItem.id);
      }
  
      const newArray =  [...data.slice(0, idx), newItem, ...data.slice(idx + 1)];
      return {data: newArray}
    });
  }

  onCloseModal = () => {
    this.setState((state) => {
      const currentModalState = (state.modal) ? false : true
      return { modal: currentModalState }
    });
  }

  onAddToCartConfirm = () => {
    const { cartItem } = this.state;
    if(cartItem){
      const { id } = cartItem;
      writeToLocalStorage("cart", id);
      this.onCloseModal();
    }
  }

  render(){
    const { data, modal } = this.state;
    if(!data)
      return <div>No data yet</div>
    return (
      <div className="App">
        <ProductList data={data} onAddToCart={ this.onAddToCart } onChooseItem={ this.toogleChooseItem }/>
        <MyModal 
          showModal={modal} 
          onAddToCartConfirm={this.onAddToCartConfirm} 
          onCloseModal={this.onCloseModal}/>
      </div>
    )
  }
}
