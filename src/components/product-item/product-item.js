import React, { Component } from 'react'
import './product-item.css';

export default class ProductItem extends Component{

    render(){
        const { data, onAddToCart, onChooseItem } = this.props;
        const chosen = (data.isChosen) ? " chosen" : "";
        return (
            <div className="card product-item" style={ {width: '18rem'}}>
                <img className="card-img-top product-item-img" src={data.imgUrl} alt="lol"></img>
                <div className="card-body">
                    <h5 className="card-title">{data.name}</h5>
                    <p className="card-text">{data.price}</p>
                    <p className="card-text">{data.article}</p>
                    <p className="card-text">{data.color}</p>
                    <div className="d-flex justify-content-between">
                        <button onClick={ onAddToCart }>Додати в корзину</button>
                        <div className={`product-item-chosen-button ${chosen}`} onClick={ onChooseItem }>
                            <i className="las la-star"></i>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}