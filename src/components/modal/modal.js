import React from 'react';
import PropTypes from './node_modules/prop-types';
import './modal.css';

debugger

export default class Modal extends React.Component {
    static propTypes = {
        header: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired
    };

    render() {

        return (<>
            <span onClick={this.props.onClick} className="close"></span>
            <div className="modal-content">
                <div className="modal-content container">
                    <div className="modal-header">
                        <span onClick={this.props.onClick} className="close">&times;</span>
                        <h2>{this.props.header}</h2>
                    </div>
                    <div className="modal-body">
                        <h2>{this.props.text}</h2>

                    </div>
                    <div className="modal-footer">
                        <h3>
                            <button className='button' style={{backgroundColor: this.props.style}}
                                    onClick={this.props.onClick}>Close
                            </button>
                            <button onClick={this.props.onClick}>SUBMIT</button>
                        </h3>
                    </div>
                </div>

            </div>
            <div style={{color: "red"}} className="modal" onClick={this.props.onClick}>

            </div>
        </>);
    }


}
